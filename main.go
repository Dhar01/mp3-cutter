package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
)

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter the path to the MP3 file:")
	inputFile, err := reader.ReadString('\n')
	check(err)

	inputFile = strings.TrimSpace(inputFile)

	// loading the song from same directory
	file, err := os.Open(inputFile)
	check(err)

	streamer, format, err := mp3.Decode(file)
	check(err)
	defer streamer.Close()

	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))
	speaker.Play(streamer)
	select {}
}
