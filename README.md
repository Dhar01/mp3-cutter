# Mp3-cutter

As part of my Golang learning journey, I want to create a mp3 cutter in golang.

The program will
- Load an mp3 file.
- Show prompt user for the starting time.
- Show prompt user for the ending time.
- Give user the end-result.

I want to set a project, learn to work with golang, git & gitlab properly with TDD.

# TO-DO

### Command Line

First, I will create a command line application for this.

- [ ] Taking user input.



### UI

UI is the second part!